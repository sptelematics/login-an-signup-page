from login.models import Login
from django import forms

class LoginForm(forms.ModelForm):
    class Meta:
        model=Login
        fields=['username','password']
        widgets={
            'username':forms.TextInput(attrs={'placeholder':"Username"}),
            'password':forms.PasswordInput(attrs={'placeholder':"enter password"}),
        }