from django.shortcuts import render,redirect
from login.forms import LoginForm
from login.models import Login
from django.contrib.auth.models import auth
from django.contrib import messages

# Create your views here.
def login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            obj=Login.objects.create(username=username,password=password)
            obj.save()
            user = auth.authenticate(username=username, password=password)
            if user is not None:
                auth.login(request,user)
                return redirect('home')
            else:
                messages.info(request, 'invalid username and password')
                return redirect('login')
    else:
        form = LoginForm()
    return render(request, 'login.html', {'form': form})


def get_data(request):
    get_data=Login.objects.all()
    return render(request,'display.html',{'get_data':get_data})
