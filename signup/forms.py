
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class SignupForm(UserCreationForm):
    emailid=forms.EmailField()
    class Meta:
        model=User
        fields=['username','emailid','password1','password2']
        widgets={
            'username':forms.TextInput(attrs={'placeholder':"username"}),
            'emailid':forms.EmailInput(attrs={'placeholder':"enter email id"}),
            'password1':forms.PasswordInput(attrs={'placeholder':"enter password"}),
            'password2':forms.PasswordInput(attrs={'placeholder':"comfirmation password"}),
        }