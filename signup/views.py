from django.shortcuts import render,redirect
from signup.forms import SignupForm
from django.contrib.auth.models import User
from django.contrib import messages

# Create your views here.

     
def signup(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password1 = request.POST['password1']
            password2 = request.POST['password2']
            emailid = request.POST['emailid']
            user = User.objects.create_user(username=username, password=password1, email=emailid)
            user.save()
            messages.success(request, "successfully create registeration")
            return redirect('login')
    else:
        form = SignupForm()
    return render(request, 'signup.html', {'form': form})
